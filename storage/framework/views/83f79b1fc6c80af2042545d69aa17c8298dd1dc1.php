<?php $__env->startSection('content-top'); ?>
    <?php echo $__env->make('base::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('base::partials.ubicacion', ['ubicacion' => ['Boletin']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Boletin.',
        'columnas' => [

        ]
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]); ?>

            <?php echo $Boletin->generate(); ?>

			<div class="form-group col-md-3">
				<label for="notas">¿Aprobo?</label>
				<select class="form-control" name="notas">
					<option value="">- Seleccione un valor</option>
					<option value="true">Sí</option>
					<option value="false">No</option>
				</select>
			</div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(isset($layouts) ? $layouts : 'base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>