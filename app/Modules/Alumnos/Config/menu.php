<?php

$menu['alumnos'] = [
    [
        'nombre'        => 'Colegio',
        'dirección'     => '#Colegio',
        'icono'         => 'fa fa-note',
        'menu'          => [
            [
				'nombre' 	=> 'Niveles',
				'direccion' => 'alumnos/grados',
				'icono' 	=> 'fa fa-user'
			],
			[
				'nombre' 	=> 'Representantes',
				'direccion' => 'alumnos/representantes',
				'icono' 	=> 'fa fa-users'
			],
			[
				'nombre' 	=> 'Alumnos',
				'direccion' => 'alumnos/alumnos',
				'icono' 	=> 'fa fa-users'
			],
			[
				'nombre' 	=> 'Boletín',
				'direccion' => 'alumnos/boletin',
				'icono' 	=> 'fa fa-users'
			],
        ]
    ]
]

 ?>
