<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;

use App\Modules\Alumnos\Models\Niveles;
use App\Modules\Alumnos\Models\Alumnos;

class Boletin extends Modelo
{
    protected $table = 'boletin';
    protected $fillable = ["nivel_id","notas","alumno_id"];
    protected $campos = [
        'nivel_id' => [
            'type' => 'select',
            'label' => 'Nivel',
            'placeholder' => '- Seleccione un Nivel',
            'url' => 'Agrega una URL Aqui!'
        ],
        'alumno_id' => [
            'type' => 'select',
            'label' => 'Alumno',
            'placeholder' => '- Seleccione un Alumno',
            'url' => 'Agrega una URL Aqui!'
        ],
        // 'aprobado' => [
        //     'type' => 'checkbox',
        //     'label' => '¿Aprobo el nivel cursado?',
        // ],
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['alumno_id']['options'] = Alumnos::pluck('nombre', 'id');
        $this->campos['nivel_id']['options'] = Niveles::pluck('nivel', 'id');
    }

    public function niveles()
	{
		return $this->belongsTo('App\Modules\Alumnos\Models\Niveles');
	}
    public function alumnos()
	{
		return $this->belongsTo('App\Modules\Alumnos\Models\Alumnos');
	}



}
