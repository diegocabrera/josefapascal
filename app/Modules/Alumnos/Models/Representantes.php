<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;



class Representantes extends Modelo
{
    protected $table = 'representantes';
    protected $fillable = ["nombre","dni"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Representantes'
    ],
    'dni' => [
        'type' => 'text',
        'label' => 'Dni',
        'placeholder' => 'Dni del Representantes'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}