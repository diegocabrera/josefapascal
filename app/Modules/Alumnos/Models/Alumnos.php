<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;

use App\Modules\Alumnos\Models\Representantes;
use App\Modules\Alumnos\Models\Niveles;



class Alumnos extends Modelo
{
    protected $table = 'alumnos';
    protected $fillable = ["nombre","representante_id","nivel_id"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Alumnos'
    ],
    'representante_id' => [
        'type' => 'select',
        'label' => 'Representante',
        'placeholder' => '- Seleccione un Representante',
        'url' => 'Agrega una URL Aqui!'
    ],
    'nivel_id' => [
        'type' => 'select',
        'label' => 'Nivel',
        'placeholder' => '- Seleccione un Nivel',
        'url' => 'Agrega una URL Aqui!'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
		$this->campos['nivel_id']['options'] = Niveles::pluck('nivel', 'id');
        $this->campos['representante_id']['options'] = Representantes::pluck('nombre', 'id');

    }
}
