<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;



class Niveles extends Modelo
{
    protected $table = 'niveles';
    protected $fillable = ["nivel"];
    protected $campos = [
    'nivel' => [
        'type' => 'text',
        'label' => 'Nivel',
        'placeholder' => 'Nivel del Niveles'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }


}
