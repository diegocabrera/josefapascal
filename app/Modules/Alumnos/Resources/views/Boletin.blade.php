@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')

    @include('base::partials.ubicacion', ['ubicacion' => ['Boletin']])

    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Boletin.',
        'columnas' => [

        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Boletin->generate() !!}
			<div class="form-group col-md-3">
				<label for="notas">¿Aprobo?</label>
				<select class="form-control" name="notas">
					<option value="">- Seleccione un valor</option>
					<option value="true">Sí</option>
					<option value="false">No</option>
				</select>
			</div>
        {!! Form::close() !!}
    </div>
@endsection
