@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Niveles']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Niveles.',
        'columnas' => [
            'Nivel' => '100'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Niveles->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection