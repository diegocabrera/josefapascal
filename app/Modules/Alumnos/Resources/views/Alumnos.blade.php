@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Alumnos']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Alumnos.',
        'columnas' => [
            'Nombre' => '33.333333333333',
		'Representante' => '33.333333333333',
		'Nivel' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Alumnos->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection