<?php

namespace App\Modules\Alumnos\Http\Controllers;

//Controlador Padre
use App\Modules\Alumnos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Alumnos\Http\Requests\NivelesRequest;

//Modelos
use App\Modules\Alumnos\Models\Niveles;

class NivelesController extends Controller
{
    protected $titulo = 'Niveles';

    public $js = [
        'Niveles'
    ];
    
    public $css = [
        'Niveles'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('alumnos::Niveles', [
            'Niveles' => new Niveles()
        ]);
    }

    public function nuevo()
    {
        $Niveles = new Niveles();
        return $this->view('alumnos::Niveles', [
            'layouts' => 'base::layouts.popup',
            'Niveles' => $Niveles
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Niveles = Niveles::find($id);
        return $this->view('alumnos::Niveles', [
            'layouts' => 'base::layouts.popup',
            'Niveles' => $Niveles
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Niveles = Niveles::withTrashed()->find($id);
        } else {
            $Niveles = Niveles::find($id);
        }

        if ($Niveles) {
            return array_merge($Niveles->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(NivelesRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Niveles = $id == 0 ? new Niveles() : Niveles::find($id);

            $Niveles->fill($request->all());
            $Niveles->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Niveles->id,
            'texto' => $Niveles->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Niveles::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Niveles::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Niveles::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Niveles::select([
            'id', 'nivel', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}