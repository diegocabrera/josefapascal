<?php

namespace App\Modules\Alumnos\Http\Controllers;

//Controlador Padre
use App\Modules\Alumnos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Alumnos\Http\Requests\AlumnosRequest;

//Modelos
use App\Modules\Alumnos\Models\Alumnos;

class AlumnosController extends Controller
{
    protected $titulo = 'Alumnos';

    public $js = [
        'Alumnos'
    ];
    
    public $css = [
        'Alumnos'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('alumnos::Alumnos', [
            'Alumnos' => new Alumnos()
        ]);
    }

    public function nuevo()
    {
        $Alumnos = new Alumnos();
        return $this->view('alumnos::Alumnos', [
            'layouts' => 'base::layouts.popup',
            'Alumnos' => $Alumnos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Alumnos = Alumnos::find($id);
        return $this->view('alumnos::Alumnos', [
            'layouts' => 'base::layouts.popup',
            'Alumnos' => $Alumnos
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Alumnos = Alumnos::withTrashed()->find($id);
        } else {
            $Alumnos = Alumnos::find($id);
        }

        if ($Alumnos) {
            return array_merge($Alumnos->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(AlumnosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Alumnos = $id == 0 ? new Alumnos() : Alumnos::find($id);

            $Alumnos->fill($request->all());
            $Alumnos->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Alumnos->id,
            'texto' => $Alumnos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Alumnos::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Alumnos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Alumnos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Alumnos::select([
            'id', 'nombre', 'representante_id', 'nivel_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}