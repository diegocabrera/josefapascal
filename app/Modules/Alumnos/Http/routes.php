<?php

Route::group(['middleware' => 'web', 'prefix' => 'alumnos', 'namespace' => 'App\\Modules\Alumnos\Http\Controllers'], function()
{
    Route::get('/', 'AlumnosController@index');

        Route::group(['prefix' => 'alumnos'], function() {
            Route::get('/',                 'AlumnosController@index');
            Route::get('nuevo',             'AlumnosController@nuevo');
            Route::get('cambiar/{id}',      'AlumnosController@cambiar');

            Route::get('buscar/{id}',       'AlumnosController@buscar');

            Route::post('guardar',          'AlumnosController@guardar');
            Route::put('guardar/{id}',      'AlumnosController@guardar');

            Route::delete('eliminar/{id}',  'AlumnosController@eliminar');
            Route::post('restaurar/{id}',   'AlumnosController@restaurar');
            Route::delete('destruir/{id}',  'AlumnosController@destruir');

            Route::get('datatable',         'AlumnosController@datatable');
        });


        Route::group(['prefix' => 'representantes'], function() {
            Route::get('/',                 'RepresentantesController@index');
            Route::get('nuevo',             'RepresentantesController@nuevo');
            Route::get('cambiar/{id}',      'RepresentantesController@cambiar');

            Route::get('buscar/{id}',       'RepresentantesController@buscar');

            Route::post('guardar',          'RepresentantesController@guardar');
            Route::put('guardar/{id}',      'RepresentantesController@guardar');

            Route::delete('eliminar/{id}',  'RepresentantesController@eliminar');
            Route::post('restaurar/{id}',   'RepresentantesController@restaurar');
            Route::delete('destruir/{id}',  'RepresentantesController@destruir');

            Route::get('datatable',         'RepresentantesController@datatable');
        });

        Route::group(['prefix' => 'boletin'], function() {
            Route::get('/',                 'BoletinController@index');
            Route::get('nuevo',             'BoletinController@nuevo');
            Route::get('cambiar/{id}',      'BoletinController@cambiar');

            Route::get('buscar/{id}',       'BoletinController@buscar');

            Route::post('guardar',          'BoletinController@guardar');
            Route::put('guardar/{id}',      'BoletinController@guardar');

            Route::delete('eliminar/{id}',  'BoletinController@eliminar');
            Route::post('restaurar/{id}',   'BoletinController@restaurar');
            Route::delete('destruir/{id}',  'BoletinController@destruir');

            Route::get('datatable',         'BoletinController@datatable');
        });


        Route::group(['prefix' => 'niveles'], function() {
            Route::get('/',                 'NivelesController@index');
            Route::get('nuevo',             'NivelesController@nuevo');
            Route::get('cambiar/{id}',      'NivelesController@cambiar');

            Route::get('buscar/{id}',       'NivelesController@buscar');

            Route::post('guardar',          'NivelesController@guardar');
            Route::put('guardar/{id}',      'NivelesController@guardar');

            Route::delete('eliminar/{id}',  'NivelesController@eliminar');
            Route::post('restaurar/{id}',   'NivelesController@restaurar');
            Route::delete('destruir/{id}',  'NivelesController@destruir');

            Route::get('datatable',         'NivelesController@datatable');
        });

    //{{route}}
});
