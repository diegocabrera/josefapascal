<?php

namespace App\Modules\Alumnos\Http\Requests;

use App\Http\Requests\Request;

class ProfesoresEspecialesRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:255'], 
		'dni' => ['required', 'min:3', 'max:255']
	];
}