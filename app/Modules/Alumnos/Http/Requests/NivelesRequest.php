<?php

namespace App\Modules\Alumnos\Http\Requests;

use App\Http\Requests\Request;

class NivelesRequest extends Request {
    protected $reglasArr = [
		'nivel' => ['required', 'min:3', 'max:255']
	];
}