<?php

namespace App\Modules\Alumnos\Http\Requests;

use App\Http\Requests\Request;

class AlumnosRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:255'], 
		'representante_id' => ['required', 'integer'], 
		'nivel_id' => ['required', 'integer']
	];
}