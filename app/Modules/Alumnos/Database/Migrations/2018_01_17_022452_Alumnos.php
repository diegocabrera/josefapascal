<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alumnos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('niveles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nivel');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('representantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('dni');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('alumnos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('representante_id')->unsigned();
            $table->integer('nivel_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('representante_id')
                  ->references('id')->on('representantes')
                  ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('nivel_id')
                ->references('id')->on('niveles')
                ->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::create('boletin', function(Blueprint $table){
            $table->increments('id');
            $table->integer('nivel_id')->nulleable()->unsigned();
            $table->string('notas');
            $table->integer('alumno_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('nivel_id')
                ->references('id')->on('niveles')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('alumno_id')
                ->references('id')->on('alumnos')
                ->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin');
        Schema::dropIfExists('asignaturas');
        Schema::dropIfExists('alumnos');
        Schema::dropIfExists('profesores');
        Schema::dropIfExists('profesores_especiales');
        Schema::dropIfExists('representantes');
        Schema::dropIfExists('grados');
    }
}
